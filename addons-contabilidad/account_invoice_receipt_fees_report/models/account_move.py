# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
#import logging
#_logger=logging.getLogger(__name__)

class AccountMove(models.Model):
    _inherit = "account.move"

    is_receipt_of_fees_flag = fields.Boolean(string="Recibo por Honorario",compute="compute_campo_is_receipt_of_fees_flag",store=True)
    #### CAMPOS EXTRA DE RXH ################
    retencion = fields.Float(string="Monto Retención",compute="compute_campo_retencion",store=True)


    @api.depends('journal_id','journal_id.invoice_type_code_id')
    def compute_campo_is_receipt_of_fees_flag(self):
        for rec in self:
            rec.is_receipt_of_fees_flag = False

            if rec.journal_id and rec.journal_id.invoice_type_code_id == '02':
                rec.is_receipt_of_fees_flag = True



    @api.depends('journal_id','company_id','company_id.receipt_of_fees_account_id',
        'line_ids','is_receipt_of_fees_flag')
    def compute_campo_retencion(self):
        for rec in self:
            rec.retencion = 0.00

            if rec.line_ids and rec.company_id.receipt_of_fees_account_id and rec.is_receipt_of_fees_flag:
                move_line_retencion_id = rec.line_ids.filtered(lambda e:e.account_id == rec.company_id.receipt_of_fees_account_id)
                if move_line_retencion_id:
                    rec.retencion = abs(move_line_retencion_id[0].balance)
