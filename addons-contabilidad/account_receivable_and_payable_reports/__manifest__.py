{
    "name": "Reporte de Cuentas por Cobrar y Pagar a Fecha Actual",
    'version': "1.0.0",
    'author': 'Franco Najarro',
    'website':'',
    'category':'Accounting',
    'description':'''
        Reporte de Cuentas por Cobrar y Pagar a Fecha Actual.
        ''',
    "depends": ['base','account','bo_pe_contabilidad_documents','extra_account_move_line'],
    "data": [
        'security/ir.model.access.csv',
        'views/account_payable_report_current_date_view.xml',
        'views/account_receivable_report_current_date_view.xml',
    ]
}
