from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning, RedirectWarning
import re
import logging
_logger = logging.getLogger(__name__)

class AccountPayment(models.Model):
    _inherit = "account.payment"

    sunat_table_01_id = fields.Many2one('sunat.table.01',string="Tipo Medio de Pago SUNAT",
        domain="[('active','=',True)]")